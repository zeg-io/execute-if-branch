# Execute-if-branch

This CLI is designed to provide a way to conditionally perform (or prevent) subsequent commands from executing.

## Installation

```sh
npm i -g execute-if-branch

# Check version to validate installation
if-branch -v
```

### Configuration
You can set the branch name(s) to be used for the condition one of three ways.

* **Default.** By doing nothing `if-branch` performs the conditional check against the `master` branch.
* **Inline.** By passing the `-b` or `--branch` argument followed by a single branch name.
* **Configure package.json.** Add a property to your `package.json` file named `continue-if-branch`. This can either have a string or an array of strings representing the branch names to use for the condition check. 

## Usage

`if-branch [options] <command to execute>`

### Examples

Execute command if the branch matches:

```sh
# ᚶ master
$ if-branch echo "The branch is, in fact, master."
# output
The branch is, in fact, master.
```

Execute tests if on master, then echo a message

```sh
# ᚶ master
$ if-branch npm test && echo "Success."

# ~If tests succeed~
# output
Success.
```

Do not execute tests if on master, then echo a message

```sh
# ᚶ master
$ if-branch -n npm test && echo "Success."

# Condition is met so the command is not run.
# output
Success.

```

If you want a series of commands to run, but only if the condition is not met.

```sh
# ᚶ master
$ if-branch -n npm "test && echo 'Success.'"

# Condition is met so the command is not run.
# no-output

```

#### Options

|option|default|description|
|:---|:---|:---|
|-b, --branch|master|Pass in the branch to base the condition on|
|-n, --do-not-execute| _false_|If true, do not execute|
