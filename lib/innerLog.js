const chalk = require('chalk')

module.exports = (message) => {
  process.stdout.write(`${chalk.cyan('if-branch: cmd:')} ${message}`)
}
