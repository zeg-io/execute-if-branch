#!/usr/bin/env node
const yargs = require('yargs'),
  fs = require('fs'),
  path = require('path')
const currentGitBranch = require('current-git-branch'),
  chalk = require('chalk')
const { spawn } = require('child_process')

const log = require('../lib/log')
const ilog = require('../lib/innerLog')
let pkg, continueIfBranch

if (fs.existsSync('./package.json')) {
  pkg = JSON.parse(fs.readFileSync('./package.json', 'utf8'))
  continueIfBranch = pkg['continue-if-branch']
} else
  console.info(path.join(__dirname, './package.json'))
// const pkg = require(path.join(__dirname, './package.json'))

const options = yargs
  .usage('Usage: [options] <command>')
  .option('b', { alias: 'branch', describe: 'Branch name' })
  .option('n', { alias: 'do-not-execute', describe: "Dont't execute if true" })
  .option('v', { alias: 'version', describe: "gets the if-branch version" })
  .argv

const branch = currentGitBranch()

if (!branch) log(chalk.red('This folder is not a git repo, or has no origin.'))
if (!options._ || options._.length === 0) {
  log(chalk.red('A command must follow if-branch. "if-branch --help" for more info.'))
  return
}

if (options.branch || continueIfBranch) {
  if (options.branch) {
    continueIfBranch = [ options.branch ]
  } else if (typeof continueIfBranch === 'string') {
    continueIfBranch = [ continueIfBranch ]
  } else if (!Array.isArray(continueIfBranch)) {
    // not a string and not an array, not set up
    log('The \'continue-if-branch\' property in the package.json file should be a string or an array.')
    process.exit(1)
  }
} else {
  log(chalk.yellow("No \'continue-if-branch\' property found in the package.json file; using 'master' instead"))
  continueIfBranch = [ 'master' ]
}
if (continueIfBranch.includes(branch)) {
  log(`[${chalk.greenBright(branch)}] branch condition met...`)
  if (options.doNotExecute) {
    log(chalk.yellow(`[${chalk.greenBright('Do Not Execute')}] condition met.`))
  } else {
    log('executing...')
    const [cmd, ...args] = options._
    const executingCommand = args.length > 0 ? spawn(cmd, args) : spawn(cmd)

    executingCommand.stdout.on('data', (data) => {
      ilog(`${data}\n`)
    })
    executingCommand.stderr.on('data', (data) => {
      ilog(`error executing: \`${options._.join(' ')}\`\n\n${chalk.red(data)}\n`)
      process.exit(1)
    })
    executingCommand.on('close', code => {
      log('Done.')
    })
  }
} else {
  log(chalk.yellow(`Current branch [ ${chalk.cyan(branch)} ] does not match [ ${chalk.cyan(continueIfBranch)} ].`))
}
